import 'package:get/get.dart';
import 'package:mobile/app/controller/emailHp.dart';

class EmailHpBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(EmailHpController());
  }
}
