import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:mobile/app/binding/absen.dart';
import 'package:mobile/app/binding/emailHp.dart';
import 'package:mobile/app/binding/home.dart';
import 'package:mobile/app/binding/jadwal.dart';
import 'package:mobile/app/binding/login.dart';
import 'package:mobile/app/binding/password.dart';
import 'package:mobile/app/binding/pembayaran.dart';
import 'package:mobile/app/binding/pengumumuan.dart';
import 'package:mobile/app/binding/resetPass.dart';
import 'package:mobile/app/binding/startBinding.dart';
import 'package:mobile/app/binding/tugas.dart';
import 'package:mobile/app/binding/tunggakan.dart';
import 'package:mobile/app/ui/absensi.dart';
import 'package:mobile/app/ui/emailHp.dart';
import 'package:mobile/app/ui/home.dart';
import 'package:mobile/app/ui/jadwal.dart';
import 'package:mobile/app/ui/login.dart';
import 'package:mobile/app/ui/password.dart';
import 'package:mobile/app/ui/pembayaran.dart';
import 'package:mobile/app/ui/pengumuman.dart';
import 'package:mobile/app/ui/resetPass.dart';
import 'package:mobile/app/ui/splashscreen.dart';
import 'package:mobile/app/ui/tugas.dart';
import 'package:mobile/app/ui/tunggakan.dart';

// declare pages
List<GetPage> pages = [
  GetPage<SplashScreen>(
    name: "/",
    page: () => SplashScreen(),
    binding: StartBinding(),
  ),
  GetPage<Login>(
    name: "/login",
    page: () => Login(),
    binding: LoginBinding(),
  ),
  GetPage<Home>(
    name: "/home",
    page: () => Home(),
    binding: HomeBinding(),
  ),
  GetPage<Absensi>(
    name: "/absensi",
    page: () => Absensi(),
    binding: AbsensiBinding(),
  ),
  GetPage<JadwalPelajaran>(
    name: "/jadwal",
    page: () => JadwalPelajaran(),
    binding: JadwalBinding(),
  ),
  GetPage<Tunggakan>(
    name: "/tunggakan",
    page: () => Tunggakan(),
    binding: TunggakanBinding(),
  ),
  GetPage<Pembayaran>(
    name: "/pembayaran",
    page: () => Pembayaran(),
    binding: PembayaranBinding(),
  ),
  GetPage<Password>(
    name: "/password",
    page: () => Password(),
    binding: PasswordBinding(),
  ),
  GetPage<EmailHp>(
    name: "/emailHp",
    page: () => EmailHp(),
    binding: EmailHpBinding(),
  ),
  GetPage<ResetPass>(
    name: "/resetPass",
    page: () => ResetPass(),
    binding: ResetPassBinding(),
  ),
  GetPage<Pengumuman>(
    name: "/pengumuman",
    page: () => Pengumuman(),
    binding: PengumumanBinding(),
  ),
  GetPage<Tugas>(
    name: "/tugas",
    page: () => Tugas(),
    binding: TugasBinding(),
  ),
];
