import 'package:get/get.dart';
import 'package:mobile/app/controller/absen.dart';

// create AbsensiBinding to bind to bind controller with ui/page
class AbsensiBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AbsensiController(), fenix: true);
  }
}
