class Tugas {
  String? mapel;
  String? judul;
  String? deskripsi;
  DateTime? createdAt;

  Tugas({this.mapel, this.judul, this.deskripsi, this.createdAt});

  factory Tugas.fromJson(Map<String, dynamic> json) {
    DateTime tgl = DateTime.parse(json["created_at"]);
    return Tugas(
      mapel: json["mapel"],
      judul: json["judul"],
      deskripsi: json["deskripsi"],
      createdAt: DateTime.utc(
        tgl.year,
        tgl.month,
        tgl.day,
        tgl.hour,
        tgl.minute,
        tgl.second,
      ).toLocal(),
    );
  }
}

List<Tugas> parseTugas(dynamic tugas) {
  return List<Tugas>.from(
    tugas.map(
      (item) => Tugas.fromJson(item),
    ),
  );
}
