import 'package:get/get.dart';
import 'package:mobile/app/controller/password.dart';

class PasswordBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(PasswordController());
  }
}
