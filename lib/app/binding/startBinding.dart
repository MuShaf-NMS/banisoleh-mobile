import 'package:get/get.dart';
import 'package:mobile/app/controller/connection.dart';
import 'package:mobile/app/controller/splashScreen.dart';

class StartBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ConnectionController());
    Get.put(SSController());
  }
}
