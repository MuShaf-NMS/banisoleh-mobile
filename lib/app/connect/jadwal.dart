import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/url.dart';

class JadwalConnection extends GetConnect {
  var storage = GetStorage();
  Future<Response> getJadwal(int hari) {
    final user = storage.read("user");
    return get(
      "$url/mobile/jadwal/$hari",
      headers: {"Authorization": "Bearer ${user["accessToken"]}"},
    );
  }
}
