import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/emailHp.dart';
import 'package:mobile/app/controller/connection.dart';
import 'package:mobile/app/controller/user.dart';
import 'package:mobile/app/widget.dart/Dialog.dart';

class EmailHpController extends GetxController {
  EmailHpConnect emailHpConnect = EmailHpConnect();
  ConnectionController connectionController = Get.find();
  UserController userController = Get.find();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  var storage = GetStorage();
  var email = "".obs;
  var hp = "".obs;

  Future updateEmailHp() async {
    if (await connectionController.checkConnection()) {
      Get.dialog(LoadingDialog(), barrierDismissible: false);
      final res = await emailHpConnect.update(email.value, hp.value);
      if (!res.hasError) {
        await userController.updateUser(email.value, hp.value);
        Get.back(closeOverlays: true);
        Get.snackbar(
          "Sukses",
          res.body["msg"],
          snackPosition: SnackPosition.TOP,
        );
      } else {
        if (Get.isOverlaysOpen) Get.back();
        if (res.status.connectionError) {
          Get.snackbar(
            "Koneksi error",
            "Silahkan coba lagi",
            snackPosition: SnackPosition.TOP,
          );
        } else {
          if (res.statusCode == 401) {
            Get.dialog(ExpiredTokenDialog(), barrierDismissible: false);
          } else if (res.statusCode == 400) {
            Get.snackbar(
              "Gagal",
              res.body["msg"],
              snackPosition: SnackPosition.TOP,
            );
          } else {
            Get.snackbar(
              "Terjadi Kesalahan",
              res.statusText ?? "",
              snackPosition: SnackPosition.TOP,
            );
          }
        }
      }
    } else {
      Get.snackbar(
        "Error",
        "Tidak ada internet",
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  String? validatorEmail(String? _email) {
    if (_email!.isEmpty) {
      return 'Email tidak boleh kosong';
    }
    return null;
  }

  void onChangedEmail(String _email) {
    email.value = _email;
  }

  String? validatorHp(String? _hp) {
    if (_hp!.isEmpty) {
      return 'Email tidak boleh kosong';
    }
    return null;
  }

  void onChangedHp(String _hp) {
    hp.value = _hp;
  }

  @override
  void onInit() {
    super.onInit();
    var user = storage.read("user");
    email.value = user["email"];
    hp.value = user["hp"];
  }
}
