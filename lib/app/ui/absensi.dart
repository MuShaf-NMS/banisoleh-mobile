import "package:flutter/material.dart";
import 'package:get/get.dart';
import 'package:mobile/app/controller/absen.dart';
import 'package:mobile/app/utils/date.dart';
import 'package:mobile/app/widget.dart/table.dart';

// create absensi UI and set it controller
class Absensi extends GetView<AbsensiController> {
  final List<String> headers = ["Hari", "Tanggal", "Keterangan"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Absensi"),
      ),
      body: Obx(
        () => Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              children: [
                Container(
                  height: (Get.size.height - kToolbarHeight) / 2,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Kehadiran ananda Hari ini:",
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                      Text(
                        dateLongId.format(DateTime.now()),
                        style: TextStyle(fontSize: 17),
                      ),
                      SizedBox(height: 10),
                      Text(
                        controller.toDay.value == "A"
                            ? "Alpa"
                            : controller.toDay.value == "S"
                                ? "Sakit"
                                : controller.toDay.value == "I"
                                    ? "Izin"
                                    : controller.toDay.value == "H"
                                        ? "Hadir"
                                        : "Libur",
                        style: TextStyle(
                          fontStyle: FontStyle.italic,
                          fontSize: 25,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                        width: 50,
                        child: Container(
                          color: Color(controller.toDay.value == "A"
                              ? 0xFFFF0000
                              : controller.toDay.value == "S"
                                  ? 0xFFFF8800
                                  : controller.toDay.value == "I"
                                      ? 0xFF1188FF
                                      : controller.toDay.value == "H"
                                          ? 0xFF00FF00
                                          : 0xFFaa9955),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  child: AbsensiTable(
                    headers: headers,
                    absensi: controller.absensi,
                  ),
                )
              ],
            )),
      ),
    );
  }
}
