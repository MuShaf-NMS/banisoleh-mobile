import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/controller/connection.dart';
import 'package:mobile/app/notification/notification.dart';

class SSController extends GetxController {
  var storage = GetStorage();
  ConnectionController connectionController = Get.find();
  @override
  void onReady() async {
    super.onReady();
    String? fcmToken = "";
    while (fcmToken == "") {
      if (await connectionController.checkConnection()) {
        fcmToken = await FirebaseMessaging.instance.getToken();
        storage.write("fcmToken", fcmToken);
        FirebaseMessaging.onMessage.listen(notificationHandler);
        FirebaseMessaging.onMessageOpenedApp.listen(openHandler);
        FirebaseMessaging.onBackgroundMessage(backgroundHandler);
        Get.offNamed(storage.hasData("user") ? "/home" : "/login");
      }
    }
  }
}
