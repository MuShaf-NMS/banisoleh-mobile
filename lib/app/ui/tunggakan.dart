import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/app/controller/tunggakan.dart';
import 'package:mobile/app/utils/number.dart';

class Tunggakan extends GetView<TunggakanController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tunggakan"),
      ),
      body: Obx(
        () => Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: DropdownButtonFormField(
                  value: controller.thnAjr.value,
                  items: List.from(
                    controller.daftarThnAjr.map(
                      (t) => DropdownMenuItem(
                        child: Text(t),
                        value: t.replaceAll(RegExp(r"/"), "-"),
                      ),
                    ),
                  ),
                  decoration: InputDecoration(
                    labelText: "Tahun Ajar",
                  ),
                  onChanged: controller.onChangedThnAjr,
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: ((controller.tunggakan.length) / 2).ceil(),
                  itemBuilder: (BuildContext context, int index) => Container(
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(color: Color(0xFFf58634)),
                      ),
                    ),
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Container(
                              width: (Get.size.width / 2) - 15,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(controller
                                      .tunggakan[(index * 2)].tunggakan),
                                  Text(rupiah.format(
                                      controller.tunggakan[(index * 2)].value)),
                                ],
                              ),
                            ),
                            (((index * 2) + 1) == (controller.tunggakan.length))
                                ? Container(width: (Get.size.width / 2) - 15)
                                : Container(
                                    width: (Get.size.width / 2) - 15,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(controller
                                            .tunggakan[(index * 2) + 1]
                                            .tunggakan),
                                        Text(rupiah.format(controller
                                            .tunggakan[(index * 2) + 1].value)),
                                      ],
                                    ),
                                  ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
