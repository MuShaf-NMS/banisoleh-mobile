import 'package:get/get.dart';
import 'package:mobile/app/controller/notification.dart';
import 'package:mobile/app/controller/thnAjr.dart';
import 'package:mobile/app/controller/user.dart';

class UserBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(UserController());
    Get.put(NotificationController());
    Get.put(ThnAjrController());
  }
}
