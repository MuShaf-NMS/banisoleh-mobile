import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/app/controller/jadwal.dart';

class JadwalPelajaran extends GetView<JadwalController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Jadwal Pelajaran"),
      ),
      body: Obx(
        () => Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: DropdownButtonFormField(
                  value: controller.hari.value,
                  items: List.from(
                    controller.daftarHari.map(
                      (h) => DropdownMenuItem(
                        child: Text(h),
                        value: controller.daftarHari.indexOf(h),
                      ),
                    ),
                  ),
                  decoration: InputDecoration(
                    labelText: "Hari",
                  ),
                  onChanged: controller.onChangedHari,
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: (controller.jadwal.length / 2).ceil(),
                  itemBuilder: (BuildContext contect, int index) => Container(
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(color: Color(0xFFf58634)),
                      ),
                    ),
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Container(
                              width: (Get.size.width / 2) - 15,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(controller.jadwal[index * 2].mapel),
                                  Text(controller.jadwal[index * 2].guru),
                                  Text(
                                      "${controller.jadwal[index * 2].dari} - ${controller.jadwal[index * 2].sampai}"),
                                ],
                              ),
                            ),
                            (((index * 2) + 1) == (controller.jadwal.length))
                                ? Container(width: (Get.size.width / 2) - 15)
                                : Container(
                                    width: (Get.size.width / 2) - 15,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(controller
                                            .jadwal[(index * 2) + 1].mapel),
                                        Text(controller
                                            .jadwal[(index * 2) + 1].guru),
                                        Text(
                                            "${controller.jadwal[(index * 2) + 1].dari} - ${controller.jadwal[(index * 2) + 1].sampai}"),
                                      ],
                                    ),
                                  )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
