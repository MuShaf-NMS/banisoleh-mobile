import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/pengumuman.dart';
import 'package:mobile/app/controller/connection.dart';
import 'package:mobile/app/controller/notification.dart';
import 'package:mobile/app/model/pengumuman.dart';
import 'package:mobile/app/widget.dart/Dialog.dart';

class PengumumanController extends GetxController {
  PengumumanConnection pengumumanConnection = PengumumanConnection();
  ConnectionController connectionController = Get.find();
  NotificationController notificationController = Get.find();
  var storage = GetStorage();
  var pengumuman = [].obs;

  Future getPengumuman() async {
    if (await connectionController.checkConnection()) {
      Get.dialog(LoadingDialog(), barrierDismissible: false);
      final res = await pengumumanConnection.getPengumuman();
      if (!res.hasError) {
        pengumuman.value = List.from(parsePengumuman(res.body).reversed);
        storage.write("Pengumuman", 0);
        if (Get.isOverlaysOpen) Get.back();
      } else {
        if (Get.isOverlaysOpen) Get.back();
        if (res.status.connectionError) {
          Get.snackbar(
            "Koneksi error",
            "Silahkan coba lagi",
            snackPosition: SnackPosition.TOP,
          );
        } else {
          if (res.statusCode == 401) {
            Get.dialog(ExpiredTokenDialog(), barrierDismissible: false);
          } else if (res.statusCode == 400) {
            Get.snackbar(
              "Gagal",
              res.body["msg"],
              snackPosition: SnackPosition.TOP,
            );
          } else {
            Get.snackbar(
              "Terjadi Kesalahan",
              res.statusText ?? "",
              snackPosition: SnackPosition.TOP,
            );
          }
        }
      }
    } else {
      Get.snackbar(
        "Error",
        "Tidak ada internet",
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  @override
  Future onReady() async {
    super.onReady();
    await getPengumuman();
  }
}
