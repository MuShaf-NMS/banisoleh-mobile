import 'package:get/get.dart';
import 'package:mobile/app/controller/resetPass.dart';

class ResetPassBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ResetPassController());
  }
}
