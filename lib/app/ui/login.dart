import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';
import 'package:mobile/app/controller/login.dart';

class Login extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Obx(
        () => Container(
          padding: EdgeInsets.all(10),
          child: Form(
            key: controller.formKey,
            child: ListView(
              children: [
                SizedBox(height: 50),
                Image.asset(
                  'images/sibas.png',
                  height: 150,
                ),
                SizedBox(height: 50),
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  initialValue: controller.nis.value,
                  style: TextStyle(fontSize: 20),
                  onChanged: controller.onChangedNis,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    hintText: 'NIS',
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  validator: controller.validatorNis,
                ),
                SizedBox(height: 20),
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  onChanged: controller.onChangedPassword,
                  style: TextStyle(fontSize: 20),
                  decoration: InputDecoration(
                    hintText: 'Password',
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.green),
                      borderRadius: BorderRadius.circular(30),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    suffixIcon: IconButton(
                      onPressed: () => controller.showPassword(),
                      icon: Icon(controller.visibility.value
                          ? Icons.visibility
                          : Icons.visibility_off),
                    ),
                  ),
                  obscureText: !controller.visibility.value,
                  validator: controller.validatorPassword,
                ),
                SizedBox(height: 20),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: Get.size.width / 4),
                  child: ElevatedButton(
                    child: Text(
                      'Login',
                      style: TextStyle(fontSize: 20),
                    ),
                    style: ElevatedButton.styleFrom(
                      // primary: Color(0xFF00A859),
                      primary: Color(0xFFf58634),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                    onPressed: () async {
                      if (controller.formKey.currentState!.validate()) {
                        await controller.login();
                      }
                    },
                  ),
                ),
                SizedBox(height: 10),
                GestureDetector(
                  child: Text(
                    "Lupa password?",
                    style: TextStyle(fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                  onTap: () {
                    Get.toNamed("/resetPass");
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
