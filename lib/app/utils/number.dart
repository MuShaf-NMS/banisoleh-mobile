import 'package:intl/intl.dart';

// declare number formatter
final rupiah =
    NumberFormat.currency(locale: "id", symbol: "Rp", decimalDigits: 0);
