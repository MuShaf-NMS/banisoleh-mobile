import 'package:get/get.dart';
import 'package:mobile/app/controller/pembayaran.dart';

class PembayaranBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PembayaranController(), fenix: true);
  }
}
