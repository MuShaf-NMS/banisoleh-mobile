import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/password.dart';
import 'package:mobile/app/controller/connection.dart';
import 'package:mobile/app/widget.dart/Dialog.dart';

class PasswordController extends GetxController {
  PasswordConnect passwordConnect = PasswordConnect();
  ConnectionController connectionController = Get.find();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  var storage = GetStorage();
  var lama = "".obs;
  var baru = "".obs;
  var confirm = "".obs;
  var visibleLama = false.obs;
  var visibleBaru = false.obs;

  Future updatePassword() async {
    if (await connectionController.checkConnection()) {
      Get.dialog(LoadingDialog(), barrierDismissible: false);
      final res = await passwordConnect.update(lama.value, baru.value);
      if (!res.hasError) {
        Get.back(closeOverlays: true);
        Get.snackbar(
          "Sukses",
          res.body["msg"],
          snackPosition: SnackPosition.TOP,
        );
      } else {
        if (Get.isOverlaysOpen) Get.back();
        if (res.status.connectionError) {
          Get.snackbar(
            "Koneksi error",
            "Silahkan coba lagi",
            snackPosition: SnackPosition.TOP,
          );
        } else {
          if (res.statusCode == 401) {
            Get.dialog(ExpiredTokenDialog(), barrierDismissible: false);
          } else if (res.statusCode == 400) {
            Get.snackbar(
              "Gagal",
              res.body["msg"],
              snackPosition: SnackPosition.TOP,
            );
          } else {
            Get.snackbar(
              "Terjadi Kesalahan",
              res.statusText ?? "",
              snackPosition: SnackPosition.TOP,
            );
          }
        }
      }
    } else {
      Get.snackbar(
        "Error",
        "Tidak ada internet",
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  String? validatorLama(String? _lama) {
    if (_lama!.isEmpty) {
      return 'Password lama tidak boleh kosong';
    }
    return null;
  }

  void onChangedLama(String _lama) {
    lama.value = _lama;
  }

  String? validatorBaru(String? _baru) {
    if (_baru!.isEmpty) {
      return 'Password baru tidak boleh kosong';
    } else if (_baru == lama.value) {
      return 'Password baru tidak boleh sama dengan password lama';
    }
    return null;
  }

  void onChangedBaru(String _baru) {
    baru.value = _baru;
  }

  String? validatorConfirm(String? _confirm) {
    if (_confirm != baru.value) {
      return 'Konfirmasi password baru anda';
    }
    return null;
  }

  void onChangedConfirm(String _confirm) {
    confirm.value = _confirm;
  }

  void showLama() {
    visibleLama.value = !visibleLama.value;
  }

  void showBaru() {
    visibleBaru.value = !visibleBaru.value;
  }
}
