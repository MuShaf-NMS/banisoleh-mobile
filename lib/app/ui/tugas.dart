import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:mobile/app/controller/tugas.dart';
import 'package:mobile/app/widget.dart/card.dart';

class Tugas extends GetView<TugasController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tugas"),
      ),
      body: Obx(
        () => Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: ListView.builder(
            itemCount: controller.tugas.length,
            itemBuilder: (BuildContext context, int index) => TugasCard(
              mapel: controller.tugas[index].mapel,
              judul: controller.tugas[index].judul,
              deskripsi: controller.tugas[index].deskripsi,
              waktu: controller.tugas[index].createdAt,
            ),
          ),
        ),
      ),
    );
  }
}
