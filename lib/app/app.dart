import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:mobile/app/binding/startBinding.dart';
import 'package:mobile/app/route/route.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialBinding: StartBinding(),
      initialRoute: "/",
      debugShowCheckedModeBanner: false,
      localizationsDelegates: GlobalMaterialLocalizations.delegates,
      theme: ThemeData(
        // appBarTheme: AppBarTheme(backgroundColor: Color(0xFF00A859)),
        appBarTheme: AppBarTheme(backgroundColor: Color(0xFFf58634)),
      ),
      getPages: pages,
    );
    // );
  }
}
