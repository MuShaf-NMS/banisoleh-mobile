import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Color(0xFFF58634),
      // backgroundColor: Color(0xFF629D62),
      // backgroundColor: Color(0xFF008000),
      backgroundColor: Color(0xFFe3e2db),
      body: Container(
        // color: Color(0xFF1E747C),
        // color: Color(0xFF365535),
        // color: Color(0xFFF58634),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'images/sibas.png',
              width: 150,
            ),
            SizedBox(height: 15),
            CircularProgressIndicator(
              color: Color(0xFFf58634),
            )
          ],
        ),
      ),
    );
  }
}
