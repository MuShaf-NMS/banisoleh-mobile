import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/app/controller/pembayaran.dart';
import 'package:mobile/app/utils/date.dart';
import 'package:mobile/app/utils/number.dart';
import 'package:mobile/app/widget.dart/table.dart';

class Pembayaran extends GetView<PembayaranController> {
  final List<String> headers = ["Tanggal", "Pembayaran", "Nominal"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("History Pembayaran"),
      ),
      body: Obx(
        () => Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: DropdownButtonFormField(
                  value: controller.thnAjr.value,
                  items: List.from(
                    controller.daftarThnAjr.map(
                      (t) => DropdownMenuItem(
                        child: Text(t),
                        value: t.replaceAll(RegExp(r"/"), "-"),
                      ),
                    ),
                  ),
                  decoration: InputDecoration(
                    labelText: "Tahun Ajar",
                  ),
                  onChanged: controller.onChangedThnAjr,
                ),
              ),
              CustomTable(
                headers: headers,
                items: controller.pembayaran
                    .map(
                      (e) => {
                        "Tanggal": dateId.format(e.waktu),
                        "Pembayaran": e.ket,
                        "Nominal": rupiah.format(e.nominal),
                      },
                    )
                    .toList(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
