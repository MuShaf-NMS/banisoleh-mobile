import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/jadwal.dart';
import 'package:mobile/app/controller/connection.dart';
import 'package:mobile/app/model/jadwal.dart';
import 'package:mobile/app/widget.dart/Dialog.dart';

class JadwalController extends GetxController {
  JadwalConnection jadwalConnection = JadwalConnection();
  ConnectionController connectionController = Get.find();
  var storage = GetStorage();
  var jadwal = [].obs;
  var hari = (DateTime.now().weekday - 1).obs;
  var daftarHari =
      ["Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Ahad"].obs;

  Future getJadwal() async {
    if (await connectionController.checkConnection()) {
      Get.dialog(LoadingDialog(), barrierDismissible: false);
      final res = await jadwalConnection.getJadwal(hari.value);
      if (!res.hasError) {
        jadwal.value = parseJadwal(res.body);
        if (Get.isOverlaysOpen) Get.back();
      } else {
        if (Get.isOverlaysOpen) Get.back();
        if (res.status.connectionError) {
          Get.snackbar(
            "Koneksi error",
            "Silahkan coba lagi",
            snackPosition: SnackPosition.TOP,
          );
        } else {
          if (res.statusCode == 401) {
            Get.dialog(ExpiredTokenDialog(), barrierDismissible: false);
          } else if (res.statusCode == 400) {
            Get.snackbar(
              "Gagal",
              res.body["msg"],
              snackPosition: SnackPosition.TOP,
            );
          } else {
            Get.snackbar(
              "Terjadi Kesalahan",
              res.statusText ?? "",
              snackPosition: SnackPosition.TOP,
            );
          }
        }
      }
    } else {
      Get.snackbar(
        "Error",
        "Tidak ada internet",
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  void onChangedHari(dynamic _hari) {
    hari.value = _hari;
    getJadwal();
  }

  @override
  Future onReady() async {
    super.onReady();
    await getJadwal();
  }
}
