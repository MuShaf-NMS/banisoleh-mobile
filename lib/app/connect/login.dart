import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/url.dart';

class LoginConnection extends GetConnect {
  var storage = GetStorage();
  Future<Response> login(String nis, String password, String? token) {
    return post("$url/mobile/login", {
      "nis": nis,
      "password": password,
      "token": token,
    });
  }

  Future<Response> logout() {
    final user = storage.read("user");
    return get(
      "$url/logout",
      headers: {"Authorization": "Bearer ${user["accessToken"]}"},
    );
  }
}
