import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/url.dart';

// create class AbsensiConnection to connect to absensi api
class AbsensiConnection extends GetConnect {
  // get data from local storage with GetStorage
  var storage = GetStorage();
  // create methode to get absensi
  Future<Response> getAbsensi() {
    // get user from local storage
    final user = storage.read("user");
    return get(
      "$url/mobile/absensi",
      // "$url/absensi",
      headers: {"Authorization": "Bearer ${user["accessToken"]}"},
    );
  }
}
