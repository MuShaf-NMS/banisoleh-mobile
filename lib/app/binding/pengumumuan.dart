import 'package:get/get.dart';
import 'package:mobile/app/controller/pengumuman.dart';

class PengumumanBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PengumumanController(), fenix: true);
  }
}
