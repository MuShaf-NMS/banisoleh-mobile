import 'package:get/get.dart';
import 'package:mobile/app/controller/tunggakan.dart';

class TunggakanBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => TunggakanController(), fenix: true);
  }
}
