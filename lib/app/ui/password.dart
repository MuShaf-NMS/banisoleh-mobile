import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/app/controller/password.dart';

class Password extends GetView<PasswordController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ganti Password"),
      ),
      body: Obx(
        () => Container(
          padding: EdgeInsets.all(10),
          child: Form(
            key: controller.formKey,
            child: ListView(
              children: [
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  style: TextStyle(fontSize: 20),
                  onChanged: controller.onChangedLama,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    hintText: 'Password lama',
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    suffixIcon: IconButton(
                      onPressed: () => controller.showLama(),
                      icon: Icon(controller.visibleLama.value
                          ? Icons.visibility
                          : Icons.visibility_off),
                    ),
                  ),
                  obscureText: !controller.visibleLama.value,
                  validator: controller.validatorLama,
                ),
                SizedBox(height: 15),
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  style: TextStyle(fontSize: 20),
                  onChanged: controller.onChangedBaru,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    hintText: 'Password baru',
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    suffixIcon: IconButton(
                      onPressed: () => controller.showBaru(),
                      icon: Icon(controller.visibleBaru.value
                          ? Icons.visibility
                          : Icons.visibility_off),
                    ),
                  ),
                  obscureText: !controller.visibleBaru.value,
                  validator: controller.validatorBaru,
                ),
                SizedBox(height: 15),
                TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    style: TextStyle(fontSize: 20),
                    onChanged: controller.onChangedConfirm,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'Konfirmasi password baru',
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      suffixIcon: Icon(Icons.visibility_off),
                    ),
                    obscureText: true,
                    validator: controller.validatorConfirm),
                SizedBox(height: 15),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: Get.size.width / 4),
                  child: ElevatedButton(
                    child: Text(
                      'Simpan',
                      style: TextStyle(fontSize: 20),
                    ),
                    style: ElevatedButton.styleFrom(
                      // primary: Color(0xFF00A859),
                      primary: Color(0xFFf58634),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                    onPressed: () async {
                      if (controller.formKey.currentState!.validate()) {
                        await controller.updatePassword();
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
