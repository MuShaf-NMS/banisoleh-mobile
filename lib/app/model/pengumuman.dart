class Pengumuman {
  String? judul;
  String? isi;
  DateTime? createdAt;

  Pengumuman({this.judul, this.isi, this.createdAt});

  factory Pengumuman.fromJson(Map<String, dynamic> json) {
    DateTime tgl = DateTime.parse(json["created_at"]);
    return Pengumuman(
      judul: json["judul"],
      isi: json["isi"],
      createdAt: DateTime.utc(
        tgl.year,
        tgl.month,
        tgl.day,
        tgl.hour,
        tgl.minute,
        tgl.second,
      ).toLocal(),
    );
  }
}

List<Pengumuman> parsePengumuman(dynamic pengumuman) {
  return List<Pengumuman>.from(
    pengumuman.map(
      (item) => Pengumuman.fromJson(item),
    ),
  );
}
