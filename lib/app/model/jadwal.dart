class Mapel {
  String? mapel;
  String? guru;
  String? dari;
  String? sampai;

  Mapel({this.mapel, this.guru, this.dari, this.sampai});

  factory Mapel.fromJson(Map<String, dynamic> json) {
    return Mapel(
      mapel: json["mapel"],
      guru: json["guru"],
      dari: json["dari_jam"],
      sampai: json["sampai_jam"],
    );
  }
}

List<Mapel> parseJadwal(dynamic jadwal) {
  return List<Mapel>.from(jadwal.map((item) => Mapel.fromJson(item)));
}
