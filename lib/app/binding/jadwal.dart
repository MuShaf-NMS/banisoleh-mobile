import 'package:get/get.dart';
import 'package:mobile/app/controller/jadwal.dart';

class JadwalBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => JadwalController(), fenix: true);
  }
}
