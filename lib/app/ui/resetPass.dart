import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/app/controller/resetPass.dart';

class ResetPass extends GetView<ResetPassController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Reset Password"),
      ),
      body: Obx(
        () => Container(
          padding: EdgeInsets.all(10),
          child: Form(
            key: controller.formKey,
            child: ListView(
              children: [
                TextFormField(
                  initialValue: controller.nis.value,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  style: TextStyle(fontSize: 20),
                  onChanged: controller.onChangedNis,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    hintText: 'NIS',
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  validator: controller.validatorNis,
                ),
                SizedBox(height: 15),
                TextFormField(
                  initialValue: controller.email.value,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  style: TextStyle(fontSize: 20),
                  onChanged: controller.onChangedEmail,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    hintText: 'Email',
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  validator: controller.validatorEmail,
                ),
                SizedBox(height: 15),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: Get.size.width / 4),
                  child: ElevatedButton(
                    child: Text(
                      'Kirim',
                      style: TextStyle(fontSize: 20),
                    ),
                    style: ElevatedButton.styleFrom(
                      // primary: Color(0xFF00A859),
                      primary: Color(0xFFf58634),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                    onPressed: () async {
                      if (controller.formKey.currentState!.validate()) {
                        await controller.resetPassword();
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
