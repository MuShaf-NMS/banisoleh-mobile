import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/pembayaran.dart';
import 'package:mobile/app/controller/connection.dart';
import 'package:mobile/app/controller/thnAjr.dart';
import 'package:mobile/app/model/pembayaran.dart';
import 'package:mobile/app/widget.dart/Dialog.dart';

class PembayaranController extends GetxController {
  PembayaranConnection pembayaranConnection = PembayaranConnection();
  ConnectionController connectionController = Get.find();
  ThnAjrController thnAjrController = Get.find();
  var storage = GetStorage();
  var pembayaran = [].obs;
  var thnAjr = "".obs;
  var daftarThnAjr = [].obs;

  setThnAjr() async {
    if (thnAjrController.thnAjr.length == 0) await thnAjrController.getThnAjr();
    thnAjr.value = thnAjrController.thnAjr.first.replaceAll(RegExp(r"/"), "-");
    daftarThnAjr = thnAjrController.thnAjr;
  }

  Future getPembayaran() async {
    if (await connectionController.checkConnection()) {
      Get.dialog(LoadingDialog(), barrierDismissible: false);
      final res = await pembayaranConnection.getPembayaran(thnAjr.value);
      if (!res.hasError) {
        pembayaran.value = parsePembayaran(res.body);
        if (Get.isOverlaysOpen) Get.back();
      } else {
        if (Get.isOverlaysOpen) Get.back();
        if (res.status.connectionError) {
          Get.snackbar(
            "Koneksi error",
            "Silahkan coba lagi",
            snackPosition: SnackPosition.TOP,
          );
        } else {
          if (res.statusCode == 401) {
            Get.dialog(ExpiredTokenDialog(), barrierDismissible: false);
          } else if (res.statusCode == 400) {
            Get.snackbar(
              "Gagal",
              res.body["msg"],
              snackPosition: SnackPosition.TOP,
            );
          } else {
            Get.snackbar(
              "Terjadi Kesalahan",
              res.statusText ?? "",
              snackPosition: SnackPosition.TOP,
            );
          }
        }
      }
    } else {
      Get.snackbar(
        "Error",
        "Tidak ada internet",
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  void onChangedThnAjr(dynamic _thnAjr) {
    thnAjr.value = _thnAjr;
    getPembayaran();
  }

  @override
  Future onReady() async {
    super.onReady();
    await setThnAjr();
    await getPembayaran();
  }
}
