import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/absen.dart';
import 'package:mobile/app/controller/connection.dart';
import 'package:mobile/app/model/absen.dart';
import 'package:mobile/app/widget.dart/Dialog.dart';

// create class AbsensiController
class AbsensiController extends GetxController {
  // declare absensi connection
  AbsensiConnection absensiConnection = AbsensiConnection();
  ConnectionController connectionController = Get.find();
  var storage = GetStorage();
  var absensi = [].obs;
  var toDay = "".obs;

  // create methode getAbsensi
  Future getAbsensi() async {
    // check connection
    if (await connectionController.checkConnection()) {
      // show loading dialog
      Get.dialog(LoadingDialog(), barrierDismissible: false);
      final res = await absensiConnection.getAbsensi();
      // chack if response has error
      if (!res.hasError) {
        DateTime now = DateTime.now();
        absensi.value = parseAbsen(res.body);
        toDay.value = absensi
            .firstWhere(
              (e) => e.tgl == DateTime(now.year, now.month, now.day),
              orElse: () => Absen(ket: "L"),
            )
            .ket;
        // close dialog if opened
        if (Get.isOverlaysOpen) Get.back();
      } else {
        if (Get.isOverlaysOpen) Get.back();
        // check connection to server
        if (res.status.connectionError) {
          Get.snackbar(
            "Koneksi error",
            "Silahkan coba lagi",
            snackPosition: SnackPosition.TOP,
          );
        } else {
          // if res.statusCode == 401 show ExpiredTokenDialog
          if (res.statusCode == 401) {
            Get.dialog(ExpiredTokenDialog(), barrierDismissible: false);
          } else {
            Get.snackbar(
              "Terjadi Kesalahan",
              res.statusText ?? "",
              snackPosition: SnackPosition.TOP,
            );
          }
        }
      }
    } else {
      Get.snackbar(
        "Error",
        "Tidak ada internet",
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  @override
  Future onReady() async {
    super.onReady();
    await getAbsensi();
  }
}
