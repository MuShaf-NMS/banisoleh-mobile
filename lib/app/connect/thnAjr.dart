import 'package:get/get.dart';
import 'package:mobile/app/connect/url.dart';
import 'package:mobile/app/model/thnAjr.dart';

class ThnAjrConnect extends GetConnect {
  Future<Response> getThnAjr() {
    httpClient.defaultDecoder = parseThnAjr;
    return get(
      "$url/daftar-thn-ajar",
    );
  }
}
