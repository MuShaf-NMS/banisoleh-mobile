import 'package:get/get.dart';
import 'package:mobile/app/connect/thnAjr.dart';
import 'package:mobile/app/controller/connection.dart';

class ThnAjrController extends GetxController {
  ThnAjrConnect thnAjrConnect = ThnAjrConnect();
  ConnectionController connectionController = Get.find();
  var thnAjr = [].obs;

  Future getThnAjr() async {
    if (await connectionController.checkConnection()) {
      try {
        final res = await thnAjrConnect.getThnAjr();
        if (!res.hasError) {
          thnAjr.value = res.body;
        }
      } catch (e) {
        throw e;
      }
    }
  }

  @override
  void onInit() {
    super.onInit();
    getThnAjr();
  }
}
