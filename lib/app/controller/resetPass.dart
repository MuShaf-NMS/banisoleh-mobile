import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/password.dart';
import 'package:mobile/app/controller/connection.dart';
import 'package:mobile/app/widget.dart/Dialog.dart';

class ResetPassController extends GetxController {
  PasswordConnect passwordConnect = PasswordConnect();
  ConnectionController connectionController = Get.find();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  var storage = GetStorage();
  var nis = "".obs;
  var email = "".obs;

  Future resetPassword() async {
    if (await connectionController.checkConnection()) {
      Get.dialog(LoadingDialog(), barrierDismissible: false);
      final res = await passwordConnect.reset(nis.value, email.value);
      if (!res.hasError) {
        Get.back(closeOverlays: true);
        Get.snackbar(
          "Sukses",
          res.body["msg"],
          snackPosition: SnackPosition.TOP,
        );
      } else {
        if (Get.isOverlaysOpen) Get.back();
        if (res.status.connectionError) {
          Get.snackbar(
            "Koneksi error",
            "Silahkan coba lagi",
            snackPosition: SnackPosition.TOP,
          );
        } else {
          if (res.statusCode == 401) {
            Get.dialog(ExpiredTokenDialog(), barrierDismissible: false);
          } else if (res.statusCode == 400) {
            Get.snackbar(
              "Gagal",
              res.body["msg"],
              snackPosition: SnackPosition.TOP,
            );
          } else {
            Get.snackbar(
              "Terjadi Kesalahan",
              res.statusText ?? "",
              snackPosition: SnackPosition.TOP,
            );
          }
        }
      }
    } else {
      Get.snackbar(
        "Error",
        "Tidak ada internet",
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  String? validatorNis(String? _nis) {
    if (_nis!.isEmpty) {
      return 'NIS tidak boleh kosong';
    }
    return null;
  }

  void onChangedNis(String _nis) {
    nis.value = _nis;
  }

  String? validatorEmail(String? _email) {
    if (_email!.isEmpty) {
      return 'Email tidak boleh kosong';
    }
    return null;
  }

  void onChangedEmail(String _email) {
    email.value = _email;
  }
}
