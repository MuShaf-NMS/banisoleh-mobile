import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class UserController extends GetxController {
  var storage = GetStorage();
  var nis = "".obs;
  var nama = "".obs;
  var sekolah = "".obs;
  var email = "".obs;
  var hp = "".obs;

  setUser() {
    var user = storage.read("user");
    nis.value = user["nis"];
    nama.value = user["nama"];
    sekolah.value = user["sekolah"];
    email.value = user["email"];
    hp.value = user["hp"];
  }

  Future<void> updateUser(String newEmail, String newHp) async {
    var user = storage.read("user");
    user["email"] = newEmail;
    user["hp"] = newHp;
    await storage.write("user", user);
  }

  @override
  void onInit() {
    super.onInit();
    setUser();
  }
}
