import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/route_manager.dart';
import 'package:get_storage/get_storage.dart';

// create loading dialog
class LoadingDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircularProgressIndicator(
            color: Color(0xFFf58634),
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            "Mohon tunggu ...",
            style: TextStyle(
              fontSize: 17,
            ),
          ),
        ],
      ),
    );
  }
}

// create ExpiredTokenDialog
class ExpiredTokenDialog extends StatelessWidget {
  final storage = GetStorage();
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(20),
        ),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            "Sesi ini telah berakhir",
            style: TextStyle(fontSize: 20),
          ),
          Text(
            "Mohon login kembali",
            style: TextStyle(fontSize: 20),
          ),
          TextButton(
            onPressed: () {
              storage.remove("user");
              // Get.back();
              Get.offAllNamed("/login");
            },
            child: Text(
              "Ok",
              style: TextStyle(fontSize: 20),
            ),
          )
        ],
      ),
    );
  }
}

class OpeningDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(20),
        ),
      ),
      content: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "AHLAN WA SAHLAN",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
            ),
            Text(
              "Para Penuntut Ilmu",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
            ),
            SizedBox(height: 20),
            Text(
              "Doa memohon ilmu yang bermanfaat",
              style: TextStyle(fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 10),
            Text(
              "،اَللُّهُمَّ إِنِّيْ أَسْأَلُكَ عِلْمًا نَافِعًا",
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
            ),
            Text(
              " وَ رِزْقًا طَيِّبًا وَ عَمَلًا مُتَقَبَّلًا",
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
            ),
            Text(
              "Ya Allah ... aku memohon kepada-Mu",
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
            Text(
              "ilmu yang bermanfaat, rizki yang baik",
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
            Text(
              "dan amal yang diterima",
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
            SizedBox(height: 10),
            Text(
              "HR. Ibnu Majah No. 925",
              style: TextStyle(fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }
}
