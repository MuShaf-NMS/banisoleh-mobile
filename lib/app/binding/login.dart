import 'package:get/get.dart';
import 'package:mobile/app/controller/login.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(LoginController());
  }
}
