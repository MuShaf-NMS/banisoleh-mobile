import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:mobile/app/utils/date.dart';

class PengumumanCard extends StatelessWidget {
  final String judul;
  final DateTime waktu;
  final String isi;
  PengumumanCard({required this.judul, required this.waktu, required this.isi});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xFFf58634)),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          Text(
            judul,
            style: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.w500,
            ),
          ),
          Html(data: isi),
          Container(
            alignment: Alignment.bottomRight,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(dateId.format(waktu)),
                Text(timeId.format(waktu)),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class TugasCard extends StatelessWidget {
  final String mapel;
  final String judul;
  final String deskripsi;
  final DateTime waktu;

  const TugasCard({
    required this.mapel,
    required this.judul,
    required this.deskripsi,
    required this.waktu,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xFFf58634)),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          Text(
            "$judul ($mapel)",
            style: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.w500,
            ),
          ),
          Html(data: deskripsi),
          Container(
            alignment: Alignment.bottomRight,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(dateId.format(waktu)),
                Text(timeId.format(waktu)),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
