import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/url.dart';

class TunggakanConnect extends GetConnect {
  var storage = GetStorage();

  Future<Response> getTunggakan(String thnAjr) {
    final user = storage.read("user");
    return get(
      "$url/mobile/tunggakan/${thnAjr.replaceAll(RegExp(r"/"), "-")}",
      headers: {"Authorization": "Bearer ${user["accessToken"]}"},
    );
  }
}

class SppConnect extends GetConnect {
  var storage = GetStorage();

  Future<Response> getSpp(String thnAjr) {
    final user = storage.read("user");
    return get(
      "$url/mobile/tunggakan/spp/${thnAjr.replaceAll(RegExp(r"/"), "-")}",
      headers: {"Authorization": "Bearer ${user["accessToken"]}"},
    );
  }
}

class UbConnect extends GetConnect {
  var storage = GetStorage();

  Future<Response> getUb(String thnAjr) {
    final user = storage.read("user");
    return get(
      "$url/mobile/tunggakan/ub/${thnAjr.replaceAll(RegExp(r"/"), "-")}",
      headers: {"Authorization": "Bearer ${user["accessToken"]}"},
    );
  }
}

class UbukuConnect extends GetConnect {
  var storage = GetStorage();

  Future<Response> getUbuku(String thnAjr) {
    final user = storage.read("user");
    return get(
      "$url/mobile/tunggakan/ubuku/${thnAjr.replaceAll(RegExp(r"/"), "-")}",
      headers: {"Authorization": "Bearer ${user["accessToken"]}"},
    );
  }
}

class UpConnect extends GetConnect {
  var storage = GetStorage();

  Future<Response> getUp() {
    final user = storage.read("user");
    return get(
      "$url/mobile/tunggakan/up",
      headers: {"Authorization": "Bearer ${user["accessToken"]}"},
    );
  }
}
