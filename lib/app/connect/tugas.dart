import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/url.dart';

class TugasConnection extends GetConnect {
  var storage = GetStorage();
  Future<Response> getTugas() {
    final user = storage.read("user");
    return get(
      "$url/mobile/daftar-tugas",
      headers: {"Authorization": "Bearer ${user["accessToken"]}"},
    );
  }
}
