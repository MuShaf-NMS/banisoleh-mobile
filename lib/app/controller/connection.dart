import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:get/get.dart';

class ConnectionController extends GetxController {
  Connectivity connectivity = Connectivity();
  Future<bool> checkConnection() async {
    var connection = await connectivity.checkConnectivity();
    if (connection == ConnectivityResult.none) {
      return false;
    } else {
      return true;
    }
  }
}
