import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/url.dart';

class PengumumanConnection extends GetConnect {
  var storage = GetStorage();
  Future<Response> getPengumuman() {
    final user = storage.read("user");
    return get(
      "$url/pengumuman",
      headers: {"Authorization": "Bearer ${user["accessToken"]}"},
    );
  }
}
