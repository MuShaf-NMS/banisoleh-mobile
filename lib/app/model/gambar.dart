List<String> parseGambar(dynamic images) {
  return List.from(images.map((item) => item["url"]));
}
