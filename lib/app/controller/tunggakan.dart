import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/tunggakan.dart';
import 'package:mobile/app/controller/connection.dart';
import 'package:mobile/app/controller/thnAjr.dart';
import 'package:mobile/app/model/tunggakan.dart';
import 'package:mobile/app/widget.dart/Dialog.dart';

class TunggakanController extends GetxController {
  TunggakanConnect tunggakanConnect = TunggakanConnect();
  ConnectionController connectionController = Get.find();
  ThnAjrController thnAjrController = Get.find();
  var storage = GetStorage();
  var tunggakan = [].obs;
  var thnAjr = "".obs;
  var daftarThnAjr = [].obs;

  setThnAjr() async {
    if (thnAjrController.thnAjr.length == 0) await thnAjrController.getThnAjr();
    thnAjr.value = thnAjrController.thnAjr.first.replaceAll(RegExp(r"/"), "-");
    daftarThnAjr = thnAjrController.thnAjr;
  }

  Future getTunggakan() async {
    if (await connectionController.checkConnection()) {
      Get.dialog(LoadingDialog(), barrierDismissible: false);
      final res = await tunggakanConnect.getTunggakan(thnAjr.value);
      if (!res.hasError) {
        tunggakan.value = parseTunggakan(res.body);
        if (Get.isOverlaysOpen) Get.back();
      } else {
        if (Get.isOverlaysOpen) Get.back();
        if (res.status.connectionError) {
          Get.snackbar(
            "Koneksi error",
            "Silahkan coba lagi",
            snackPosition: SnackPosition.TOP,
          );
        } else {
          if (res.statusCode == 401) {
            Get.dialog(ExpiredTokenDialog(), barrierDismissible: false);
          } else if (res.statusCode == 400) {
            Get.snackbar(
              "Gagal",
              res.body["msg"],
              snackPosition: SnackPosition.TOP,
            );
          } else {
            Get.snackbar(
              "Terjadi Kesalahan",
              res.statusText ?? "",
              snackPosition: SnackPosition.TOP,
            );
          }
        }
      }
    } else {
      Get.snackbar(
        "Error",
        "Tidak ada internet",
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  void onChangedThnAjr(dynamic _thnAjr) {
    thnAjr.value = _thnAjr;
    getTunggakan();
  }

  @override
  Future onReady() async {
    super.onReady();
    await setThnAjr();
    await getTunggakan();
  }
}
