import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/app/controller/home.dart';
import 'package:mobile/app/controller/notification.dart';
import 'package:auto_size_text/auto_size_text.dart';

class Home extends GetView<HomeController> {
  final NotificationController notificationController = Get.find();
  final TextStyle headerStyle = TextStyle(fontSize: 17);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      body: Obx(
        () => Container(
          alignment: Alignment.center,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(10),
                child: Table(
                  columnWidths: {
                    0: FlexColumnWidth(1.6),
                    1: FlexColumnWidth(0.4),
                    2: FlexColumnWidth(3)
                  },
                  children: [
                    TableRow(children: [
                      Text("NIS", style: headerStyle),
                      Text(":", style: headerStyle),
                      Text(controller.nis.value, style: headerStyle),
                    ]),
                    TableRow(children: [
                      Text("Nama", style: headerStyle),
                      Text(":", style: headerStyle),
                      Text(controller.nama.value, style: headerStyle),
                    ]),
                  ],
                ),
              ),
              CarouselSlider(
                  options: CarouselOptions(height: 200, autoPlay: true),
                  items: controller.img.length > 0
                      ? controller.img
                          .map(
                            (e) => Container(
                              padding: EdgeInsets.symmetric(horizontal: 5),
                              child: Image.network(e, loadingBuilder:
                                  (BuildContext contex, Widget child,
                                      ImageChunkEvent? loading) {
                                if (loading == null) {
                                  return child;
                                }
                                return Center(
                                  child: CircularProgressIndicator(
                                    color: Color(0xFFf58634),
                                  ),
                                );
                              }, errorBuilder: (BuildContext context,
                                  Object exception, StackTrace? stackTrace) {
                                return Icon(
                                  Icons.error,
                                  size: 50,
                                  color: Colors.grey[700],
                                );
                              }),
                            ),
                          )
                          .toList()
                      : [Image.asset("images/blank.png")]),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 10),
                  child: GridView.count(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    mainAxisSpacing: 15,
                    crossAxisSpacing: 20,
                    crossAxisCount: 3,
                    children: List<Widget>.from(
                      controller.routes.map(
                        (e) => Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Stack(
                                alignment: AlignmentDirectional.center,
                                children: [
                                  IconButton(
                                    onPressed: e.action,
                                    icon: Icon(e.icon),
                                    iconSize: 40,
                                    color: Color(0xFFf58634),
                                  ),
                                  notificationController.notif
                                              .containsKey(e.name) &&
                                          notificationController
                                                  .notif[e.name] !=
                                              0
                                      ? Positioned(
                                          right: 3,
                                          top: 3,
                                          child: Container(
                                            alignment: Alignment.center,
                                            decoration: new BoxDecoration(
                                              color: Colors.red,
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            constraints: BoxConstraints(
                                              minWidth: 17,
                                              minHeight: 17,
                                            ),
                                            child: Text(
                                              notificationController
                                                  .notif[e.name]
                                                  .toString(),
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 10,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        )
                                      : Container()
                                ],
                              ),
                              Container(
                                height: 30,
                                alignment: Alignment.center,
                                child: AutoSizeText(
                                  e.name,
                                  textAlign: TextAlign.center,
                                  maxFontSize: 15,
                                  minFontSize: 10,
                                  maxLines: 2,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
