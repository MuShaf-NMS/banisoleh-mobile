import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mobile/app/utils/date.dart';

class AbsensiTable extends StatelessWidget {
  final List<String> headers;
  final List absensi;

  AbsensiTable({required this.headers, required this.absensi});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Rekam jejak kehadiran:",
            style: TextStyle(fontSize: 17),
            textAlign: TextAlign.start,
          ),
          SizedBox(
            height: 10,
          ),
          Table(
            children: [
              TableRow(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(color: Color(0xFFf58634)),
                    top: BorderSide(color: Color(0xFFf58634)),
                  ),
                ),
                children: List<Container>.from(
                  headers.map(
                    (h) => Container(
                      padding: EdgeInsets.all(5),
                      child: Text(
                        h,
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
          Expanded(
            child: ListView.builder(
              itemBuilder: (context, index) {
                return Table(
                  border: TableBorder(
                    bottom: BorderSide(width: .5, color: Colors.black),
                  ),
                  children: [
                    TableRow(
                      children: [
                        Container(
                          padding: EdgeInsets.all(5),
                          child: Text(
                            dayId.format(absensi[index].tgl),
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          child: Text(
                            dateId.format(absensi[index].tgl),
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          child: Text(
                            absensi[index].ket == "A"
                                ? "Alpa"
                                : absensi[index].ket == "S"
                                    ? "Sakit"
                                    : absensi[index].ket == "I"
                                        ? "Izin"
                                        : absensi[index].ket == "H"
                                            ? "Hadir"
                                            : "Libur",
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                );
              },
              itemCount: absensi.length,
            ),
          )
        ],
      ),
    );
  }
}

class CustomTable extends StatelessWidget {
  final List<String> headers;
  final List<Map<String, dynamic>> items;

  CustomTable({required this.headers, required this.items});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Table(
            children: [
              TableRow(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(color: Color(0xFFf58634)),
                    top: BorderSide(color: Color(0xFFf58634)),
                  ),
                ),
                children: List<Container>.from(
                  headers.map(
                    (h) => Container(
                      padding: EdgeInsets.all(5),
                      child: Text(
                        h,
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
          Expanded(
            child: ListView.builder(
              itemBuilder: (context, index) {
                return Table(
                  border: TableBorder(
                    bottom: BorderSide(width: .5, color: Colors.black),
                  ),
                  children: [
                    TableRow(
                      children: List<Widget>.from(
                        headers.map(
                          (h) => Container(
                            // height: 30,
                            padding: EdgeInsets.all(5),
                            child: Text(
                              items[index][h],
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                );
              },
              itemCount: items.length,
            ),
          )
        ],
      ),
    );
  }
}
