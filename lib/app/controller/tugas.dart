import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/tugas.dart';
import 'package:mobile/app/controller/connection.dart';
import 'package:mobile/app/controller/notification.dart';
import 'package:mobile/app/model/tugas.dart';
import 'package:mobile/app/widget.dart/Dialog.dart';

class TugasController extends GetxController {
  TugasConnection tugasConnection = TugasConnection();
  ConnectionController connectionController = Get.find();
  NotificationController notificationController = Get.find();
  var storage = GetStorage();
  var tugas = [].obs;

  Future getTugas() async {
    if (await connectionController.checkConnection()) {
      Get.dialog(LoadingDialog(), barrierDismissible: false);
      final res = await tugasConnection.getTugas();
      if (!res.hasError) {
        tugas.value = List.from(parseTugas(res.body).reversed);
        if (Get.isOverlaysOpen) Get.back();
      } else {
        if (Get.isOverlaysOpen) Get.back();
        if (res.status.connectionError) {
          Get.snackbar(
            "Koneksi error",
            "Silahkan coba lagi",
            snackPosition: SnackPosition.TOP,
          );
        } else {
          if (res.statusCode == 401) {
            Get.dialog(ExpiredTokenDialog(), barrierDismissible: false);
          } else if (res.statusCode == 400) {
            Get.snackbar(
              "Gagal",
              res.body["msg"],
              snackPosition: SnackPosition.TOP,
            );
          } else {
            Get.snackbar(
              "Terjadi Kesalahan",
              res.statusText ?? "",
              snackPosition: SnackPosition.TOP,
            );
          }
        }
      }
    } else {
      Get.snackbar(
        "Error",
        "Tidak ada internet",
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  @override
  Future onReady() async {
    super.onReady();
    storage.write("Tugas", 0);
    await getTugas();
  }
}
