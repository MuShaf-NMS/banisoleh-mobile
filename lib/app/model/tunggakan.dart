class Tunggakan {
  String? tunggakan;
  int? value;

  Tunggakan({this.tunggakan, this.value});

  factory Tunggakan.fromSpp(Map<String, dynamic> json) {
    return Tunggakan(
      tunggakan: "SPP ${json["bulan"]}",
      value: json["tunggakan"],
    );
  }
}

List<Tunggakan> parseTunggakan(dynamic tunggakan) {
  List<Tunggakan> lTunggakan = [];
  lTunggakan.add(Tunggakan(tunggakan: "Uang Pangkal", value: tunggakan["up"]));
  lTunggakan.add(Tunggakan(tunggakan: "Uang Bangunan", value: tunggakan["ub"]));
  lTunggakan.add(Tunggakan(tunggakan: "Uang Buku", value: tunggakan["ubuku"]));
  lTunggakan.addAll(
    List<Tunggakan>.from(
      tunggakan["spp"].map((item) => Tunggakan.fromSpp(item)),
    ),
  );
  return lTunggakan;
}
