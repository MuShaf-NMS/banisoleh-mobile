import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/controller/pengumuman.dart';
import 'package:mobile/app/controller/tugas.dart';

// create notification channel
AndroidNotificationChannel channel = AndroidNotificationChannel(
  "fm_channel",
  "Firebase Notification",
  "Used for firebase notification",
  importance: Importance.max,
  playSound: true,
  sound: RawResourceAndroidNotificationSound("notif"),
);

// create notification detail
NotificationDetails notificationDetails = NotificationDetails(
  android: AndroidNotificationDetails(
    channel.id,
    channel.name,
    channel.description,
    priority: Priority.max,
    importance: channel.importance,
    playSound: channel.playSound,
    sound: channel.sound,
  ),
);

final FlutterLocalNotificationsPlugin notificationsPlugin =
    FlutterLocalNotificationsPlugin();

// create function to show notification
Future showNotification(int id, String? title, String? body) async {
  await notificationsPlugin.show(
    id,
    title,
    body,
    notificationDetails,
  );
}

Future<void> notificationHandler(RemoteMessage message) async {
  RemoteNotification? notification = message.notification;
  // await GetStorage.init();
  final storage = GetStorage();
  if (notification != null && notification.android != null) {
    String tag = notification.android?.tag ?? "";
    if (tag == "Pengumuman") {
      int pengumuman = storage.read("Pengumuman") ?? 0;
      if (Get.currentRoute == "/pengumuman") {
        PengumumanController pengumumanController = Get.find();
        await pengumumanController.getPengumuman();
      } else {
        pengumuman++;
        storage.write("Pengumuman", pengumuman);
      }
    } else {
      int tugas = storage.read("Tugas") ?? 0;
      if (Get.currentRoute == "/tugas") {
        TugasController tugasController = Get.find();
        await tugasController.getTugas();
      } else {
        tugas++;
        storage.write("Tugas", tugas);
      }
    }
    await showNotification(
      notification.hashCode,
      notification.title,
      notification.body,
    );
  }
}

Future<void> backgroundHandler(RemoteMessage message) async {
  // await Firebase.initializeApp();
  await GetStorage.init();
  final storage = GetStorage();
  RemoteNotification? notification = message.notification;
  if (notification != null) {
    String tag = notification.android?.tag ?? "";
    if (tag == "Pengumuman") {
      int pengumuman = storage.read("Pengumuman") ?? 0;
      pengumuman++;
      storage.write("Pengumuman", pengumuman);
    } else {
      int tugas = storage.read("Tugas") ?? 0;
      tugas++;
      storage.write("Tugas", tugas);
    }
  }
}

Future<void> openHandler(RemoteMessage message) async {
  RemoteNotification? notification = message.notification;
  if (notification != null) {
    String tag = notification.android?.tag ?? "";
    if (tag == "Pengumuman" && Get.currentRoute != "/pengumuman")
      Get.toNamed("/pengumuman");
    else if (tag == "Tugas" && Get.currentRoute != "/tugas")
      Get.toNamed("/tugas");
  }
}
