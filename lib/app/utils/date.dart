import 'package:intl/intl.dart';

// declare date formatter
final dayId = DateFormat("EEEE", "id");
final dateId = DateFormat("dd-MM-yyyy");
final dateLongId = DateFormat("EEEE, d MMMM yyyy", "id");
final dateTimeId = DateFormat("dd-MM-yyyy hh:mm:ss");
final dateStd = DateFormat("yyyy-MM-dd");
final timeId = DateFormat("hh:mm:ss");
