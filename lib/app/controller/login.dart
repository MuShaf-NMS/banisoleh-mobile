// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/login.dart';
import 'package:mobile/app/controller/connection.dart';
import 'package:mobile/app/widget.dart/Dialog.dart';

class LoginController extends GetxController {
  LoginConnection loginConnection = LoginConnection();
  ConnectionController connectionController = Get.find();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  // final FirebaseMessaging fm = FirebaseMessaging.instance;
  var storage = GetStorage();
  var nis = "".obs;
  var password = "".obs;
  var passController = TextEditingController();
  var visibility = false.obs;
  var loading = false.obs;
  Future login() async {
    if (await connectionController.checkConnection()) {
      Get.dialog(LoadingDialog(), barrierDismissible: false);
      // final token = await FirebaseMessaging.instance.getToken();
      final token = storage.read("fcmToken");
      final res = await loginConnection.login(nis.value, password.value, token);
      if (!res.hasError) {
        storage.write("user", res.body);
        Get.offAllNamed("/home");
        storage.write("nis", nis.value);
      } else {
        if (Get.isOverlaysOpen) Get.back();
        if (res.status.connectionError) {
          Get.snackbar(
            "Koneksi error",
            "Silahkan coba lagi",
            snackPosition: SnackPosition.TOP,
          );
        } else {
          if (res.statusCode == 401) {
            Get.dialog(ExpiredTokenDialog(), barrierDismissible: false);
          } else if (res.statusCode == 400) {
            Get.snackbar(
              "Gagal",
              res.body["msg"],
              snackPosition: SnackPosition.TOP,
            );
          } else if (res.statusCode == 403) {
            Get.snackbar(
              "Gagal",
              res.body["msg"],
              snackPosition: SnackPosition.TOP,
              // barBlur: 15,
            );
          } else {
            Get.snackbar(
              "Terjadi Kesalahan",
              res.statusText ?? "",
              snackPosition: SnackPosition.TOP,
            );
          }
        }
      }
    } else {
      Get.snackbar(
        "Error",
        "Tidak ada internet",
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  Future logout() async {
    Get.snackbar(
      "Loging out",
      "Mohon tunggu beberapa saat",
      snackPosition: SnackPosition.TOP,
    );
    try {
      await loginConnection.logout();
      storage.remove("user");
      if (Get.isOverlaysOpen) Get.back();
      Get.offNamed("/login");
    } catch (e) {
      storage.remove("user");
      if (Get.isOverlaysOpen) Get.back();
      Get.offNamed("/login");
    }
  }

  void showPassword() {
    visibility.value = !visibility.value;
  }

  String? validatorNis(String? _nis) {
    if (_nis!.isEmpty) {
      return 'NIS tidak boleh kosong';
    }
    return null;
  }

  void onChangedNis(String _nis) {
    nis.value = _nis;
  }

  String? validatorPassword(String? _password) {
    if (_password!.isEmpty) {
      return 'Password tidak boleh kosong';
    }
    return null;
  }

  void onChangedPassword(String _password) {
    password.value = _password;
  }

  @override
  void onInit() {
    nis.value = storage.read("nis") ?? "";
    super.onInit();
  }
}
