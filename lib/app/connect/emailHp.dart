import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/url.dart';

class EmailHpConnect extends GetConnect {
  var storage = GetStorage();

  Future<Response> update(String email, String hp) {
    final user = storage.read("user");
    return put(
      "$url/mobile/update-email-hp",
      {"email": email, "hp": hp},
      headers: {"Authorization": "Bearer ${user["accessToken"]}"},
    );
  }
}
