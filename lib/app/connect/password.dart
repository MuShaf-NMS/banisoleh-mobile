import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/url.dart';

class PasswordConnect extends GetConnect {
  var storage = GetStorage();

  Future<Response> update(String oldPass, String newPass) {
    final user = storage.read("user");
    return put(
      "$url/mobile/update-password",
      {"lama": oldPass, "baru": newPass},
      headers: {"Authorization": "Bearer ${user["accessToken"]}"},
    );
  }

  Future<Response> reset(String nis, String email) {
    return post(
      "$url/mobile/reset-password",
      {"nis": nis, "email": email},
    );
  }
}
