import 'package:get/get.dart';
import 'package:mobile/app/controller/home.dart';
import 'package:mobile/app/controller/notification.dart';
import 'package:mobile/app/controller/thnAjr.dart';
import 'package:mobile/app/controller/user.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(UserController());
    Get.put(ThnAjrController());
    Get.put(HomeController());
    Get.put(NotificationController());
  }
}
