import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:mobile/app/controller/pengumuman.dart';
import 'package:mobile/app/widget.dart/card.dart';

class Pengumuman extends GetView<PengumumanController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pengumuman"),
      ),
      body: Obx(
        () => Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: ListView.builder(
            itemCount: controller.pengumuman.length,
            itemBuilder: (BuildContext context, int index) => PengumumanCard(
              judul: controller.pengumuman[index].judul,
              waktu: controller.pengumuman[index].createdAt,
              isi: controller.pengumuman[index].isi,
            ),
          ),
        ),
      ),
    );
  }
}
