// create class Absen to parse absensi from api
class Absen {
  String? ket;
  DateTime? tgl;
  Absen({
    this.ket,
    this.tgl,
  });

  factory Absen.fromJson(Map<String, dynamic> json) {
    return Absen(ket: json["ket"], tgl: DateTime.tryParse(json["tgl"]));
  }
}

List<Absen> parseAbsen(dynamic absen) {
  return List.from(absen.map((item) => Absen.fromJson(item)));
}
