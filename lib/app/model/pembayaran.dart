class Pembayaran {
  String? ket;
  int? nominal;
  DateTime? waktu;

  Pembayaran({this.ket, this.nominal, this.waktu});

  factory Pembayaran.fromJson(Map<String, dynamic> json) {
    DateTime tgl = DateTime.parse(json["waktu"]);
    return Pembayaran(
      ket: json["ket"],
      nominal: int.parse(json["nominal"]),
      waktu: DateTime.utc(
        tgl.year,
        tgl.month,
        tgl.day,
        tgl.hour,
        tgl.minute,
        tgl.second,
      ).toLocal(),
    );
  }
}

List<Pembayaran> parsePembayaran(dynamic pembayaran) {
  return List<Pembayaran>.from(
    pembayaran.map(
      (item) => Pembayaran.fromJson(item),
    ),
  );
}
