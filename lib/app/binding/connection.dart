import 'package:get/get.dart';
import 'package:mobile/app/controller/connection.dart';

class ConnectionBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(ConnectionController());
  }
}
