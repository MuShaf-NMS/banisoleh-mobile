import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile/app/controller/emailHp.dart';

class EmailHp extends GetView<EmailHpController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ganti Email & HP"),
      ),
      body: Obx(
        () => Container(
          padding: EdgeInsets.all(10),
          child: Form(
            key: controller.formKey,
            child: ListView(
              children: [
                TextFormField(
                  initialValue: controller.email.value,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  style: TextStyle(fontSize: 20),
                  onChanged: controller.onChangedEmail,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    hintText: 'Email',
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  validator: controller.validatorEmail,
                ),
                SizedBox(height: 15),
                TextFormField(
                  initialValue: controller.hp.value,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  style: TextStyle(fontSize: 20),
                  onChanged: controller.onChangedHp,
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                    hintText: 'HP',
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  validator: controller.validatorHp,
                ),
                SizedBox(height: 15),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: Get.size.width / 4),
                  child: ElevatedButton(
                    child: Text(
                      'Simpan',
                      style: TextStyle(fontSize: 20),
                    ),
                    style: ElevatedButton.styleFrom(
                      // primary: Color(0xFF00A859),
                      primary: Color(0xFFf58634),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                    onPressed: () async {
                      if (controller.formKey.currentState!.validate()) {
                        await controller.updateEmailHp();
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
