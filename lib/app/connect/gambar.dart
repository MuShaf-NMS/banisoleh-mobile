import 'package:get/get.dart';
import 'package:mobile/app/connect/url.dart';
import 'package:mobile/app/model/gambar.dart';

class GambarConnect extends GetConnect {
  Future<Response> getImages() {
    httpClient.defaultDecoder = parseGambar;
    return get("$url/images");
  }
}
