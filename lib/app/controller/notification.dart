import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/controller/pengumuman.dart';
import 'package:mobile/app/controller/tugas.dart';

class NotificationController extends GetxController {
  var notif = {
    "Tugas": 0,
    "Pengumuman": 0,
  }.obs;

  @override
  void onInit() async {
    super.onInit();
    final storage = GetStorage();
    notif["Tugas"] = storage.read("Tugas") ?? 0;
    notif["Pengumuman"] = storage.read("Pengumuman") ?? 0;
    storage.listenKey("Tugas", (value) {
      if (Get.currentRoute == "/tugas") {
        TugasController tugasController = Get.find();
        tugasController.getTugas();
      }
      notif["Tugas"] = value;
    });
    storage.listenKey("Pengumuman", (value) {
      if (value != 0 && Get.currentRoute == "/pengumuman") {
        PengumumanController pengumumanController = Get.find();
        pengumumanController.getPengumuman();
      }
      notif["Pengumuman"] = value;
    });
  }
}
