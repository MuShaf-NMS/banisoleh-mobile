import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/gambar.dart';
import 'package:mobile/app/connect/login.dart';
import 'package:mobile/app/controller/connection.dart';
import 'package:mobile/app/controller/thnAjr.dart';
import 'package:mobile/app/controller/user.dart';
import 'package:mobile/app/widget.dart/Dialog.dart';
import 'package:mobile/presentation/my_flutter_app_icons.dart';

class HomeController extends GetxController {
  var storage = GetStorage();
  LoginConnection loginConnection = LoginConnection();
  GambarConnect gambarConnect = GambarConnect();
  ThnAjrController thnAjrController = Get.find();
  ConnectionController connectionController = Get.find();
  UserController userController = Get.find();
  var nis = "".obs;
  var nama = "".obs;
  var sekolah = "".obs;
  var img = [].obs;
  var routes = [].obs;

  setUser() {
    nis.value = userController.nis.value;
    nama.value = userController.nama.value;
    sekolah.value = userController.sekolah.value;
  }

  Future getGambar() async {
    var res = await gambarConnect.getImages();
    if (!res.hasError) {
      if (res.body.length > 0) {
        img.value = res.body;
      }
    } else {
      if (res.status.connectionError) {
        Get.snackbar(
          "Koneksi error",
          "Silahkan coba lagi",
          snackPosition: SnackPosition.TOP,
        );
      } else {
        if (res.statusCode == 401) {
          Get.dialog(ExpiredTokenDialog(), barrierDismissible: false);
        } else {
          Get.snackbar(
            "Terjadi Kesalahan",
            res.statusText ?? "",
            snackPosition: SnackPosition.TOP,
          );
        }
      }
    }
  }

  setRoute() {
    routes.value = [
      Route(
        name: "Tunggakan",
        icon: MyFlutterApp.bayar,
        action: () => Get.toNamed("/tunggakan"),
      ),
      Route(
        name: "History Pembayaran",
        icon: MyFlutterApp.histori_bayar,
        action: () => Get.toNamed("/pembayaran"),
      ),
      Route(
        name: "Pengumuman",
        icon: MyFlutterApp.pengumuman,
        action: () => Get.toNamed("/pengumuman"),
      ),
      Route(
        name: "Tugas",
        icon: MyFlutterApp.tugas,
        action: () => Get.toNamed("/tugas"),
      ),
      Route(
        name: "Absensi",
        icon: MyFlutterApp.absensi,
        action: () => Get.toNamed("/absensi"),
      ),
      Route(
        name: "Jadwal Pelajaran",
        icon: MyFlutterApp.jadwal_pelajaran,
        action: () => Get.toNamed("/jadwal"),
      ),
      Route(
        name: "Ganti Password",
        icon: MyFlutterApp.ganti_pasword,
        action: () => Get.toNamed("/password"),
      ),
      Route(
        name: "Ganti Email & Nomor HP",
        icon: MyFlutterApp.ganti_email,
        action: () => Get.toNamed("/emailHp"),
      ),
      Route(
        name: "Logout",
        icon: MyFlutterApp.keluar,
        action: () => logout(),
      )
    ];
  }

  @override
  void onInit() {
    super.onInit();
    setUser();
    setRoute();
  }

  @override
  void onReady() async {
    super.onReady();
    Get.dialog(OpeningDialog());
    await getGambar();
    await thnAjrController.getThnAjr();
    RemoteMessage? message =
        await FirebaseMessaging.instance.getInitialMessage();
    RemoteNotification? notification = message?.notification;
    if (notification != null) {
      String? tag = notification.android?.tag;
      if (tag == "Pengumuman")
        Get.toNamed("/pengumuman");
      else
        Get.toNamed("/tugas");
    }
  }

  Future logout() async {
    Get.dialog(LoadingDialog(), barrierDismissible: false);
    try {
      await loginConnection.logout();
      storage.remove("user");
      if (Get.isOverlaysOpen) Get.back();
      Get.offAllNamed("/login");
    } catch (e) {
      storage.remove("user");
      if (Get.isOverlaysOpen) Get.back();
      Get.offAllNamed("/login");
    }
  }
}

class Route {
  String name;
  IconData icon;
  Function action;

  Route({
    required this.name,
    required this.icon,
    required this.action,
  });
}
