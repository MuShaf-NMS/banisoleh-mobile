import 'package:get/get.dart';
import 'package:mobile/app/controller/tugas.dart';

class TugasBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => TugasController(), fenix: true);
  }
}
