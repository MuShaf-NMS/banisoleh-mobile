import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile/app/connect/url.dart';

class PembayaranConnection extends GetConnect {
  var storage = GetStorage();
  Future<Response> getPembayaran(String thnAjr) {
    final user = storage.read("user");
    return get(
      "$url/mobile/history-pembayaran/$thnAjr",
      headers: {"Authorization": "Bearer ${user["accessToken"]}"},
    );
  }
}
